package com.mycompany.example.plugin.myhook;

import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.setting.*;

public class ReviewerValidator implements RepositorySettingsValidator {

    @Override
    public void validate(Settings settings, SettingsValidationErrors errors, Repository repository) {
        if (settings.getInt("reviewers", 0) == 0) {
            errors.addFieldError("reviewers", "Number of reviewers must be greater than zero");
        }
    }
}
